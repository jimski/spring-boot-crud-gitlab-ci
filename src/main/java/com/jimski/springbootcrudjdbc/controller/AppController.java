package com.jimski.springbootcrudjdbc.controller;

//import com.jimski.springbootcrudjdbc.dao.ISalesDao;
import com.jimski.springbootcrudjdbc.entity.Sale;
//import com.jimski.springbootcrudjdbc.dao.SalesDAOImpl;
import com.jimski.springbootcrudjdbc.service.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class AppController {

    @Autowired
    //private SalesDAOImpl dao;
    private SalesService servicedao;

    @RequestMapping("/")
    public String viewHomePage (Model model)
    {
        List<Sale> list = servicedao.list();
        model.addAttribute("listSale",list);

        return ("index");
    }

    @RequestMapping("/new")
    public String showNewForm (Model model)
    {
        Sale sale = new Sale();
        model.addAttribute("saleForm",sale);

        return ("new_form");
    }

    @RequestMapping(value = "/insert",method = RequestMethod.POST)
    public String save(@ModelAttribute("saleForm") Sale sale)
    {
        servicedao.insert(sale);
        return "redirect:/";
    }


    @RequestMapping("/edit/{id}")
    public ModelAndView showEditForm(@PathVariable(name = "id") int id)
    {
        ModelAndView mv = new ModelAndView("edit_form");
        Sale sale = servicedao.get(id);

        mv.addObject("saleForm",sale);
        return mv;

    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(@ModelAttribute("saleForm") Sale sale) {
        servicedao.update(sale);
        return "redirect:/";
    }

    @RequestMapping("/delete/{id}")
    public String delete(@PathVariable(name = "id") int id) {
        servicedao.delete(id);
        return "redirect:/";
    }

}
