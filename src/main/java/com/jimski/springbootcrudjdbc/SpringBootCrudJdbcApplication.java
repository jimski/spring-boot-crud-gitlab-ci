package com.jimski.springbootcrudjdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootCrudJdbcApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootCrudJdbcApplication.class, args);
    }

}


//S$$$ystem.setProperty("server.servlet.context-path", "/crud");
