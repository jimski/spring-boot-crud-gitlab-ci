package com.jimski.springbootcrudjdbc.service;

import com.jimski.springbootcrudjdbc.dao.ISalesDao;
import com.jimski.springbootcrudjdbc.entity.Sale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SalesService {

    @Autowired
    @Qualifier("postgres")
    private ISalesDao iSalesDao;


    public List<Sale> list()
    {
        return iSalesDao.list();
    }

    public void insert (Sale sale)
    {
         iSalesDao.insert(sale);

    }

    public Sale get (int id)
    {
        return iSalesDao.get(id);
    }

    public void update(Sale sale) {
        iSalesDao.update(sale);
    }

    public void delete(int id)
    {
        iSalesDao.delete(id);
    }

}
