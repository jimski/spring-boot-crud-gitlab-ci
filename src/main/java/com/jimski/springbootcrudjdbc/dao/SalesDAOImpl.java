package com.jimski.springbootcrudjdbc.dao;

import com.jimski.springbootcrudjdbc.entity.Sale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Qualifier("postgres")
public class SalesDAOImpl implements ISalesDao{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    /*
    public SalesDAOImpl(JdbcTemplate jdbcTemplate)
    {this.jdbcTemplate=jdbcTemplate;}*/

    public List<Sale> list()
    {
        String sql = "select * from sales";
          List<Sale> listSales = jdbcTemplate.query(sql, BeanPropertyRowMapper.newInstance(Sale.class));


        return listSales;
    }

    public void insert (Sale sale)
    {
        SimpleJdbcInsert insertme = new SimpleJdbcInsert(jdbcTemplate);
        insertme.withTableName("sales").usingColumns("item","quantity","amount");
        BeanPropertySqlParameterSource param = new BeanPropertySqlParameterSource(sale);
        insertme.execute(param);
    }

    public Sale get (int id)
    {
        String sql = "SELECT * FROM SALES WHERE id = ? order by id";
        Object[] args = {id};
        Sale sale = jdbcTemplate.queryForObject(sql, args, BeanPropertyRowMapper.newInstance(Sale.class));
        return sale;    }



    public void update(Sale sale) {
        String sql = "UPDATE SALES SET item=:item, quantity=:quantity, amount=:amount WHERE id=:id";
        BeanPropertySqlParameterSource param = new BeanPropertySqlParameterSource(sale);
        NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(jdbcTemplate);
        template.update(sql, param);
    }

    public void delete(int id) {
        String sql = "DELETE FROM SALES WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }


}
