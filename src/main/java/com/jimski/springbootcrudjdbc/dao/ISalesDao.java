package com.jimski.springbootcrudjdbc.dao;

import com.jimski.springbootcrudjdbc.entity.Sale;

import java.util.List;

public interface  ISalesDao {

    public List<Sale> list();

    public void insert (Sale sale);

    public Sale get (int id);

    public void update(Sale sale) ;

    public void delete(int id);

}
