import com.jimski.springbootcrudjdbc.SpringBootCrudJdbcApplication;
import com.jimski.springbootcrudjdbc.dao.SalesDAOImpl;
import com.jimski.springbootcrudjdbc.entity.Sale;
import com.jimski.springbootcrudjdbc.service.SalesService;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.stream.Collectors;
import java.util.stream.Stream;


@SpringBootTest(classes = SpringBootCrudJdbcApplication.class)
public class SalesApplicationMockTest {

@Autowired
private SalesService salesService;

@MockBean
private SalesDAOImpl repository;

@Test
public void listTest()
{
    //Mockup Result of List() by Pass inject 4 data Test and compare it with expected number# Expected
    Mockito.when(repository.list()).thenReturn(
            Stream.of(new Sale("Rutih",100,200.90f) ,
                     new Sale("Batman",100,200.90f) ,
                    new Sale("Batman",100,200.90f) ,
                    new Sale("Ruper",200,200.90f))
                    .collect(Collectors.toList())
                    );
    assertEquals(4,salesService.list().size());


}


}
